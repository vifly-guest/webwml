msgid ""
msgstr ""
"Project-Id-Version: organization.fa\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Behrad Eslamifar <behrad_es@yahoo.com>\n"
"Language-Team: debian-l10n-persian <debian-l10n-persian@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Persian\n"
"X-Poedit-Country: IRAN, ISLAMIC REPUBLIC OF\n"
"X-Poedit-SourceCharset: utf-8\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr ""

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "اعضاء"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "مدیر"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "مدیر انتشار نسخه پایدار"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "wizard"

#: ../../english/intro/organization.data:39
msgid "chair"
msgstr ""

#: ../../english/intro/organization.data:42
msgid "assistant"
msgstr "دستیار"

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "منشی"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:66
msgid "Officers"
msgstr ""

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:90
msgid "Distribution"
msgstr ""

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:232
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:235
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:58
#: ../../english/intro/organization.data:239
msgid "Publicity team"
msgstr ""

#: ../../english/intro/organization.data:60
#: ../../english/intro/organization.data:304
msgid "Support and Infrastructure"
msgstr ""

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:62
msgid "Debian Pure Blends"
msgstr ""

#: ../../english/intro/organization.data:69
msgid "Leader"
msgstr "رهبر"

#: ../../english/intro/organization.data:71
msgid "Technical Committee"
msgstr "شورای فنی"

#: ../../english/intro/organization.data:85
msgid "Secretary"
msgstr "منشی"

#: ../../english/intro/organization.data:93
msgid "Development Projects"
msgstr "پروژه توسعه"

#: ../../english/intro/organization.data:94
msgid "FTP Archives"
msgstr "بایگانی FTP"

#: ../../english/intro/organization.data:96
#, fuzzy
#| msgid "FTP Master"
msgid "FTP Masters"
msgstr "مدیر FTP"

#: ../../english/intro/organization.data:102
msgid "FTP Assistants"
msgstr "دستیار FTP"

#: ../../english/intro/organization.data:107
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:111
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:113
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:117
msgid "Individual Packages"
msgstr ""

#: ../../english/intro/organization.data:118
msgid "Release Management"
msgstr "مدیریت انتشار"

#: ../../english/intro/organization.data:120
msgid "Release Team"
msgstr "تیم انتشار"

#: ../../english/intro/organization.data:133
msgid "Quality Assurance"
msgstr "کنترل کیفیت"

#: ../../english/intro/organization.data:134
msgid "Installation System Team"
msgstr "تیم سیستم نصب"

#: ../../english/intro/organization.data:135
msgid "Release Notes"
msgstr "نکته های انتشار"

#: ../../english/intro/organization.data:137
msgid "CD Images"
msgstr "CD Images"

#: ../../english/intro/organization.data:139
msgid "Production"
msgstr "محصول"

#: ../../english/intro/organization.data:147
msgid "Testing"
msgstr ""

#: ../../english/intro/organization.data:149
msgid "Autobuilding infrastructure"
msgstr ""

#: ../../english/intro/organization.data:151
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:159
msgid "Buildd administration"
msgstr ""

#: ../../english/intro/organization.data:178
msgid "Documentation"
msgstr ""

#: ../../english/intro/organization.data:183
msgid "Work-Needing and Prospective Packages list"
msgstr ""

#: ../../english/intro/organization.data:186
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:187
msgid "Ports"
msgstr ""

#: ../../english/intro/organization.data:222
msgid "Special Configurations"
msgstr "تنظیمات خاص"

#: ../../english/intro/organization.data:225
msgid "Laptops"
msgstr "لپ تاپ ها"

#: ../../english/intro/organization.data:226
msgid "Firewalls"
msgstr "فایروال ها"

#: ../../english/intro/organization.data:227
msgid "Embedded systems"
msgstr ""

#: ../../english/intro/organization.data:242
msgid "Press Contact"
msgstr ""

#: ../../english/intro/organization.data:244
msgid "Web Pages"
msgstr "صفحات وب"

#: ../../english/intro/organization.data:254
msgid "Planet Debian"
msgstr "سیاره دبیان"

#: ../../english/intro/organization.data:259
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:263
msgid "Debian Women Project"
msgstr ""

#: ../../english/intro/organization.data:271
msgid "Anti-harassment"
msgstr ""

#: ../../english/intro/organization.data:276
msgid "Events"
msgstr "رویدادها"

#: ../../english/intro/organization.data:282
#, fuzzy
#| msgid "Technical Committee"
msgid "DebConf Committee"
msgstr "شورای فنی"

#: ../../english/intro/organization.data:289
msgid "Partner Program"
msgstr ""

#: ../../english/intro/organization.data:294
msgid "Hardware Donations Coordination"
msgstr ""

#: ../../english/intro/organization.data:307
msgid "User support"
msgstr "پشتیبانی کاربران"

#: ../../english/intro/organization.data:374
msgid "Bug Tracking System"
msgstr ""

#: ../../english/intro/organization.data:379
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr ""

#: ../../english/intro/organization.data:387
msgid "New Members Front Desk"
msgstr ""

#: ../../english/intro/organization.data:393
msgid "Debian Account Managers"
msgstr ""

#: ../../english/intro/organization.data:397
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:398
msgid "Keyring Maintainers (PGP and GPG)"
msgstr ""

#: ../../english/intro/organization.data:402
msgid "Security Team"
msgstr "تیم امنیت"

#: ../../english/intro/organization.data:414
msgid "Consultants Page"
msgstr "صفحه مشاوره ها"

#: ../../english/intro/organization.data:419
msgid "CD Vendors Page"
msgstr "صفحه فروشندگان CD"

#: ../../english/intro/organization.data:422
msgid "Policy"
msgstr "سیاست"

#: ../../english/intro/organization.data:425
msgid "System Administration"
msgstr "مدیر سیستم"

#: ../../english/intro/organization.data:426
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""

#: ../../english/intro/organization.data:435
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""

#: ../../english/intro/organization.data:436
msgid "LDAP Developer Directory Administrator"
msgstr ""

#: ../../english/intro/organization.data:437
msgid "Mirrors"
msgstr "آینه های دریافت"

#: ../../english/intro/organization.data:444
msgid "DNS Maintainer"
msgstr "نگهدارنده DNS"

#: ../../english/intro/organization.data:445
msgid "Package Tracking System"
msgstr ""

#: ../../english/intro/organization.data:447
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:453
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:456
#, fuzzy
#| msgid "System Administration"
msgid "Salsa administrators"
msgstr "مدیر سیستم"

#: ../../english/intro/organization.data:467
msgid "Debian for children from 1 to 99"
msgstr ""

#: ../../english/intro/organization.data:470
msgid "Debian for medical practice and research"
msgstr ""

#: ../../english/intro/organization.data:473
msgid "Debian for education"
msgstr ""

#: ../../english/intro/organization.data:478
msgid "Debian in legal offices"
msgstr ""

#: ../../english/intro/organization.data:482
msgid "Debian for people with disabilities"
msgstr ""

#: ../../english/intro/organization.data:486
msgid "Debian for science and related research"
msgstr ""

#: ../../english/intro/organization.data:489
msgid "Debian for astronomy"
msgstr ""

#~ msgid "Volatile Team"
#~ msgstr "تیم Volatile"

#~ msgid "Vendors"
#~ msgstr "فروشنده ها"

#~ msgid "Security Audit Project"
#~ msgstr "تیم بررسی امنیت"

#~ msgid "Testing Security Team"
#~ msgstr "تیم تست امنیت"

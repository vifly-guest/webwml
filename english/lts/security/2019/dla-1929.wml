<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability has been discovered in php-pecl-http, the pecl_http
module for PHP 5 Extended HTTP Support.  A type confusion vulnerability
in the merge_param() function allows attackers to crash PHP and possibly
execute arbitrary code via crafted HTTP requests.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.0.4-1+deb8u1.</p>

<p>We recommend that you upgrade your php-pecl-http packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1929.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Squid, a high-performance proxy caching server for web clients, has been
found vulnerable to denial of service attacks associated with HTTP
authentication header processing.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12525">CVE-2019-12525</a>

    <p>Due to incorrect buffer management Squid is vulnerable to a denial
    of service attack when processing HTTP Digest Authentication
    credentials.</p>

    <p>Due to incorrect input validation the HTTP Request header parser for
    Digest authentication may access memory outside the allocated memory
    buffer.</p>

    <p>On systems with memory access protections this can result in the
    Squid process being terminated unexpectedly. Resulting in a denial
    of service for all clients using the proxy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12529">CVE-2019-12529</a>

    <p>Due to incorrect buffer management Squid is vulnerable to a denial
    of service attack when processing HTTP Basic Authentication
    credentials.</p>

    <p>Due to incorrect string termination the Basic authentication
    credentials decoder may access memory outside the decode buffer.</p>

    <p>On systems with memory access protections this can result in the
    Squid process being terminated unexpectedly. Resulting in a denial
    of service for all clients using the proxy.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.4.8-6+deb8u8.</p>

<p>We recommend that you upgrade your squid3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1858.data"
# $Id: $

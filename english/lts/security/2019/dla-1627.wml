<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues were fixed in Qt.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15518">CVE-2018-15518</a>

<p>A double-free or corruption during parsing of a specially crafted
illegal XML document.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19870">CVE-2018-19870</a>

<p>A malformed GIF image might have caused a NULL pointer dereference in
QGifHandler resulting in a segmentation fault.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19873">CVE-2018-19873</a>

<p>QBmpHandler had a buffer overflow via BMP data.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
5.3.2+dfsg-4+deb8u3.</p>

<p>We recommend that you upgrade your qtbase-opensource-src packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1627.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several security vulnerabilities have been discovered in symfony, a PHP
web application framework.  Numerous symfony components are affected:
Framework Bundle, Dependency Injection, Security, HttpFoundation</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10909">CVE-2019-10909</a>

    <p>Validation messages were not escaped when using the form theme of
    the PHP templating engine which, when validation messages may contain
    user input, could result in an XSS.</p>

    <p>For further information, see the upstream advisory at
    <a href="https://symfony.com/blog/cve-2019-10909-escape-validation-messages-in-the-php-templating-engine">https://symfony.com/blog/cve-2019-10909-escape-validation-messages-in-the-php-templating-engine</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10910">CVE-2019-10910</a>

    <p>Service IDs derived from unfiltered user input could result in the
    execution of any arbitrary code, resulting in possible remote code
    execution.</p>

    <p>For further information, see the upstream advisory at
    <a href="https://symfony.com/blog/cve-2019-10910-check-service-ids-are-valid">https://symfony.com/blog/cve-2019-10910-check-service-ids-are-valid</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10911">CVE-2019-10911</a>

    <p>This fixes situations where part of an expiry time in a cookie could
    be considered part of the username, or part of the username could be
    considered part of the expiry time. An attacker could modify the
    remember me cookie and authenticate as a different user. This attack
    is only possible if remember me functionality is enabled and the two
    users share a password hash or the password hashes (e.g.
    UserInterface::getPassword()) are null for all users (which is valid
    if passwords are checked by an external system, e.g. an SSO).</p>

    <p>For further information, see the upstream advisory at
    <a href="https://symfony.com/blog/cve-2019-10911-add-a-separator-in-the-remember-me-cookie-hash">https://symfony.com/blog/cve-2019-10911-add-a-separator-in-the-remember-me-cookie-hash</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10913">CVE-2019-10913</a>

    <p>HTTP methods, from either the HTTP method itself or using the
    X-Http-Method-Override header were previously returned as the method
    in question without validation being done on the string, meaning
    that they could be used in dangerous contexts when left unescaped.</p>

    <p>For further information, see the upstream advisory at
    <a href="https://symfony.com/blog/cve-2019-10913-reject-invalid-http-method-overrides">https://symfony.com/blog/cve-2019-10913-reject-invalid-http-method-overrides</a></p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.3.21+dfsg-4+deb8u5.</p>

<p>We recommend that you upgrade your symfony packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1778.data"
# $Id: $

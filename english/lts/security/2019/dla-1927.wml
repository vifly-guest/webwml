<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were found in QEMU, a fast processor emulator
(notably used in KVM and Xen HVM virtualization).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5126">CVE-2016-5126</a>

    <p>Heap-based buffer overflow in the iscsi_aio_ioctl function in
    block/iscsi.c in QEMU allows local guest OS users to cause a
    denial of service (QEMU process crash) or possibly execute
    arbitrary code via a crafted iSCSI asynchronous I/O ioctl call.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5403">CVE-2016-5403</a>

    <p>The virtqueue_pop function in hw/virtio/virtio.c in QEMU allows
    local guest OS administrators to cause a denial of service (memory
    consumption and QEMU process crash) by submitting requests without
    waiting for completion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9375">CVE-2017-9375</a>

    <p>QEMU, when built with USB xHCI controller emulator support, allows
    local guest OS privileged users to cause a denial of service
    (infinite recursive call) via vectors involving control transfer
    descriptors sequencing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12068">CVE-2019-12068</a>

    <p>QEMU scsi disk backend: lsi: exit infinite loop while executing
    script</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12155">CVE-2019-12155</a>

    <p>interface_release_resource in hw/display/qxl.c in QEMU has a NULL
    pointer dereference.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13164">CVE-2019-13164</a>

    <p>qemu-bridge-helper.c in QEMU does not ensure that a network
    interface name (obtained from bridge.conf or a --br=bridge option)
    is limited to the IFNAMSIZ size, which can lead to an ACL bypass.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14378">CVE-2019-14378</a>

    <p>ip_reass in ip_input.c in libslirp 4.0.0 has a heap-based buffer
    overflow via a large packet because it mishandles a case involving
    the first fragment.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15890">CVE-2019-15890</a>

    <p>libslirp 4.0.0, as used in QEMU, has a use-after-free in ip_reass
    in ip_input.c.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:2.1+dfsg-12+deb8u12.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1927.data"
# $Id: $

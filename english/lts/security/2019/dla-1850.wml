<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were two heap buffer overflows in the
Hyperloglog functionality provided by the Redis in-memory key-value
database.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10192">CVE-2019-10192</a>

    <p>Heap buffer overflow.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2:2.8.17-1+deb8u7.</p>

<p>We recommend that you upgrade your redis packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1850.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It has been discovered that Tor, a connection-based low-latency
anonymous communication system, contains a flaw in the hidden service
code.  A remote attacker can take advantage of this flaw to cause a
hidden service to crash with an assertion failure (TROVE-2017-005).</p>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
0.2.4.29-1.</p>

<p>We recommend that you upgrade your tor packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-982.data"
# $Id: $

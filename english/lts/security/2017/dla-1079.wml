<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Perl library for communicating with MySQL database, used in the
<q>mysql</q> commandline client is vulnerable to a man in the middle attack
in SSL configurations and remote crash when connecting to hostile
servers.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10788">CVE-2017-10788</a>

    <p>The DBD::mysql module through 4.042 for Perl allows remote
    attackers to cause a denial of service (use-after-free and
    application crash) or possibly have unspecified other impact by
    triggering (1) certain error responses from a MySQL server or (2)
    a loss of a network connection to a MySQL server. The
    use-after-free defect was introduced by relying on incorrect
    Oracle mysql_stmt_close documentation and code examples.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10789">CVE-2017-10789</a>

    <p>The DBD::mysql module through 4.042 for Perl uses the mysql_ssl=1
    setting to mean that SSL is optional (even though this setting's
    documentation has a <q>your communication with the server will be
    encrypted</q> statement), which allows man-in-the-middle attackers to
    spoof servers via a cleartext-downgrade attack, a related issue to
    <a href="https://security-tracker.debian.org/tracker/CVE-2015-3152">CVE-2015-3152</a>.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.021-1+deb7u3.</p>

<p>We recommend that you upgrade your libdbd-mysql-perl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1079.data"
# $Id: $

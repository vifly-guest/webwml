<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that Zookeeper, a service for maintaining
configuration information, didn't restrict access to the computationally
expensive wchp/wchc commands which could result in denial of service by
elevated CPU consumption.</p>

<p>This update disables those two commands by default. The new
configuration option <q>4lw.commands.whitelist</q> can be used to whitelist
commands selectively (and the full set of commands can be restored
with '*')</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.4.5+dfsg-2+deb7u1.</p>

<p>We recommend that you upgrade your zookeeper packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-986.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in cURL, an URL transfer
library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7141">CVE-2016-7141</a>

    <p>When built with NSS and the libnsspem.so library is available at
    runtime, allows an remote attacker to hijack the authentication of a
    TLS connection by leveraging reuse of a previously loaded client
    certificate from file for a connection for which no certificate has
    been set, a different vulnerability than <a href="https://security-tracker.debian.org/tracker/CVE-2016-5420">CVE-2016-5420</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7167">CVE-2016-7167</a>

    <p>Multiple integer overflows in the (1) curl_escape, (2)
    curl_easy_escape, (3) curl_unescape, and (4) curl_easy_unescape
    functions in libcurl allow attackers to have unspecified impact via
    a string of length 0xffffffff, which triggers a heap-based buffer
    overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9586">CVE-2016-9586</a>

    <p>Curl is vulnerable to a buffer overflow when doing a large floating
    point output in libcurl's implementation of the printf() functions.
    If there are any applications that accept a format string from the
    outside without necessary input filtering, it could allow remote
    attacks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16839">CVE-2018-16839</a>

    <p>Curl is vulnerable to a buffer overrun in the SASL authentication
    code that may lead to denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16842">CVE-2018-16842</a>

    <p>Curl is vulnerable to a heap-based buffer over-read in the
    tool_msgs.c:voutf() function that may result in information exposure
    and denial of service.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
7.38.0-4+deb8u13.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1568.data"
# $Id: $

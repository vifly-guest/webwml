<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15501">CVE-2018-15501</a>

      <p>A potential out-of-bounds read when processing a <q>ng</q> smart packet
      might lead to a Denial of Service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10887">CVE-2018-10887</a>

      <p>A flaw has been discovered that may lead to an integer overflow which
      in turn leads to an out of bound read, allowing to read before the
      base object. This might be used to leak memory addresses or cause a
      Denial of Service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10888">CVE-2018-10888</a>

      <p>A flaw may lead to an out-of-bound read while reading a binary delta
      file. This might result in a Denial of Service.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.21.1-3+deb8u1.</p>

<p>We recommend that you upgrade your libgit2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1477.data"
# $Id: $

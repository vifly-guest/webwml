<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-1854">CVE-2015-1854</a>

      <p>A flaw was found while doing authorization of modrdn operations.
      An unauthenticated attacker able to issue an ldapmodrdn call to
      the directory server could perform unauthorized modifications
      of entries in the directory server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15134">CVE-2017-15134</a>

      <p>Improper handling of a search filter in slapi_filter_sprintf()
      in slapd/util.c can lead to remote server crash and denial
      of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1054">CVE-2018-1054</a>

      <p>When read access on &lt;attribute_name&gt; is enabled, a flaw in
      SetUnicodeStringFromUTF_8 function in collate.c, can lead to
      out-of-bounds memory operations.
      This might result in a server crash, caused by unauthorized
      users.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1089">CVE-2018-1089</a>

      <p>Any user (anonymous or authenticated) can crash ns-slapd with a
      crafted ldapsearch query with very long filter value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10850">CVE-2018-10850</a>

      <p>Due to a race condition the server could crash in turbo mode
      (because of high traffic) or when a worker reads several requests
      in the read buffer (more_data). Thus an anonymous attacker could
      trigger a denial of service.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.3.5-4+deb8u1.</p>

<p>We recommend that you upgrade your 389-ds-base packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1428.data"
# $Id: $

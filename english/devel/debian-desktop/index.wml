#use wml::debian::template title="Debian on the Desktop"
#use wml::debian::recent_list

<h2>The Universal Operating System as your Desktop</h2>

<p>
  The Debian Desktop subproject is a group of volunteers who want to
  create the best possible operating system for home and corporate
  workstation use.  Our motto is <q>Software that Just Works</q>.  In
  short, our goal is to bring Debian, GNU, and Linux to the mainstream
  world.
  <img style="float: right;" src="debian-desktop.png" alt="Debian Desktop"/>
</p>
<h3>Our tenets</h3>
<ul>
  <li>
    Recognizing that many <a href="https://wiki.debian.org/DesktopEnvironment">Desktop Environments</a> exist, we will 
    support the use of them, and make sure they work well on Debian.
  </li>
  <li>
    We recognize that there are only two important classes of users:
    the novice, and the expert.  We will do everything we can to make
    things very easy for the novice, while allowing the expert to
    tweak things if they like.
  </li>
  <li>
    We will try to ensure that software is configured for the most
    common desktop use.  For instance, the regular user account
    added by default during installation should have permission to
    play audio and video, print, and manage the system through sudo.
  </li>
  <li>
    <p>
    We will try to ensure that questions which are asked of the user
    (which should be kept to a minimum) make sense even with a
    minimum of computer knowledge. Some Debian packages today
    present the user with difficult technical details. Technical debconf
    questions presented to the user by debian-installer should be avoided.
    To the novice, these are often confusing and frightening.
    To the expert, they may be annoying and unnecessary. A novice may not
    even know what these questions are about. An expert can configure their
    desktop environment however they like it after the installation is complete.
    The priority of these kinds of Debconf questions should be at least lowered.
    </p>
  <li>
    And we will have fun doing all of it!
  </li>
</ul>
<h3>How you can help</h3>
<p>
  The most important parts of a Debian subproject aren't mailing
  lists, web pages, or archive space for packages.  The most
  important part is <em>motivated people</em> who make things
  happen.  You don't need to be an official developer to start
  making packages and patches.  The core Debian Desktop team will
  ensure that your work is integrated.  So here's some things you
  can do:
</p>
<ul>
  <li>
    Test our <q>Desktop Default Environment</q> task (or kde-desktop task), 
    installing one of our <a href="$(DEVEL)/debian-installer/">next release 
    testing images</a> and send feedback to the <a 
    href="https://lists.debian.org/debian-desktop/">debian-desktop mailing list</a>.
  </li>
  <li>
    Work on <a href="$(DEVEL)/debian-installer/">debian-installer</a>.  
    The GTK+ frontend needs you.
  </li>
  <li>
Help <a href="https://wiki.debian.org/Teams/DebianGnome">Debian GNOME team</a>,
<a href="https://qt-kde-team.pages.debian.net/">Debian Qt and KDE Team</a> or
<a href="https://salsa.debian.org/xfce-team/">Debian Xfce Group</a>.
You can help with packaging, bug triaging, documentation, tests and more.
  </li>
  <li>
    Teach users how to install and use the Debian desktop tasks we have now
    (desktop, gnome-desktop and kde-desktop).
  </li>
  <li>
    Work on lowering the priority of, or removing unnecessary
    <a href="https://packages.debian.org/debconf">debconf</a> prompts
    from packages, and making the ones that are necessary easy to understand.
  </li>
  <li>
    Help the <a href="https://wiki.debian.org/DebianDesktop/Artwork">Debian 
    Desktop Artwork effort</a>.
  </li>
</ul>
<h3>Wiki</h3>
<p>
  We have some articles in our wiki, and our starting point there is: 
  <a href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a>. Some Debian 
  Desktop wiki articles are outdated.
</p>
<h3>Mailing List</h3>
<p>
  This subproject is being discussed in the 
  <a href="https://lists.debian.org/debian-desktop/">debian-desktop</a> mailing list.
</p>
<h3>IRC Channel</h3>
<p>
  We encourage anyone (Debian Developer or not) who is interested in
  Debian Desktop to join #debian-desktop on
  <a href="http://oftc.net/">OFTC IRC</a> (irc.debian.org).
</p>
<h3>Who's Involved?</h3>
<p>
  Anyone who wants to be is welcome. Actually, everyone in pkg-gnome, pkg-kde and pkg-xfce
  groups are indirectly involved. The debian-desktop mailing list subscribers are active
  contributors. The debian installer and tasksel groups are important for our goals too.
</p>

<p>
  This web page is maintained by <a href="https://people.debian.org/~stratus/">\
  Gustavo Franco</a>. Former maintainers were
  <a href="https://people.debian.org/~madkiss/">Martin Loschwitz</a> and 
  <a href="https://people.debian.org/~walters/">Colin Walters</a>.
</p>

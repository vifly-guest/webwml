#use wml::debian::translation-check translation="42b6b660e613679ceb4726fb1a5aebaa47b68d96" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Adskillige efterforskere har opdaget sårbarheder i den måde Intels 
processordesign har implementeret spekulativ videresendelse af data placeret i 
midlertidige mikroarkitektoniske strukturer (buffere).  Fejlen kunne gøre det 
muligt for en angriber, med kontrol over en upriviligeret proces, at læse 
følsomme oplysninger, herunder fra kernen og alle andre processer, der kører på 
systemet eller på tværs af gæst-/værtgrænser til læsning af værtshukommelse.</p>

<p>Se <a href="https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html">\
https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html</a> for 
flere oplysninger.</p>

<p>For helt at løse disse sårbarheder, er det også nødvendigt at installere 
opdateret CPU-mikrokode.  En opdateret intel-microcode-pakke (kun tilgængelige 
i Debian non-free), vil blive stillet til rådighed gennem en separat DSA.  Den 
opdaterede CPU-mikrokode kan også være tilgængelige som en del af et systems 
firmwareopdatering (<q>BIOS</q>).</p>

<p>Desuden indeholder denne opdatering en rettelse af en regression, som medførte 
deadlock inde i loopbackdriveren, hvilken opstod i forbindelse med opdateringen 
til 4.9.168 i den seneste punktopdatering af Stretch.</p>

<p>I den stabile distribution (stretch), er disse problemer rettet i
version 4.9.168-1+deb9u2.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4444.data"

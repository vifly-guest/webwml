msgid ""
msgstr ""
"Project-Id-Version: டெபியன் தள தமிழாக்கம்\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2007-10-30 17:53+0530\n"
"Last-Translator: ஆமாச்சு <amachu@ubuntu.com>\n"
"Language-Team: டெபியன் தமிழாக்கம் <vizhuthugal@anbu.in>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Tamil\n"
"X-Poedit-Country: INDIA\n"
"X-Poedit-SourceCharset: utf-8\n"

#: ../../english/international/l10n/dtc.def:10
msgid "File"
msgstr "கோப்பு"

#: ../../english/international/l10n/dtc.def:14
msgid "Package"
msgstr "பொதி"

#: ../../english/international/l10n/dtc.def:18
msgid "Score"
msgstr "மதிப்பு"

#: ../../english/international/l10n/dtc.def:22
msgid "Translator"
msgstr "மொழிபெயர்ப்பாளர்"

#: ../../english/international/l10n/dtc.def:26
msgid "Team"
msgstr "அணி"

#: ../../english/international/l10n/dtc.def:30
msgid "Date"
msgstr ""

#: ../../english/international/l10n/dtc.def:34
msgid "Status"
msgstr ""

#: ../../english/international/l10n/dtc.def:38
msgid "Strings"
msgstr ""

#: ../../english/international/l10n/dtc.def:42
msgid "Bug"
msgstr ""

#: ../../english/international/l10n/dtc.def:49
msgid "<get-var lang />, as spoken in <get-var country />"
msgstr "<get-var lang />, <get-var country /> ல் பேசப்படுவது போல்"

#: ../../english/international/l10n/dtc.def:54
msgid "Unknown language"
msgstr "தெரியாத மொழி"

#: ../../english/international/l10n/dtc.def:64
msgid "This page was generated with data collected on: <get-var date />."
msgstr "<get-var date /> மீது கொணரப் பட்ட தரவினைக் கொண்டு இப்பக்கம் ஆக்கப்பட்டது."

#: ../../english/international/l10n/dtc.def:69
msgid "Before working on these files, make sure they are up to date!"
msgstr "இக்கோப்புகளில் பணிபுரிவதற்கு முன்னர், இவை இப்போதையதுதானா என உறுதி செய்க!"

#: ../../english/international/l10n/dtc.def:79
msgid "Section: <get-var name />"
msgstr "பகுதி: <get-var name />"

#: ../../english/international/l10n/menu.def:10
msgid "L10n"
msgstr "தன்மொழியாக்கம்"

#: ../../english/international/l10n/menu.def:14
msgid "Language list"
msgstr "மொழிப் பட்டியல்"

#: ../../english/international/l10n/menu.def:18
msgid "Ranking"
msgstr "தர மதிப்பீடு"

#: ../../english/international/l10n/menu.def:22
msgid "Hints"
msgstr "துப்புக்கள்"

#: ../../english/international/l10n/menu.def:26
msgid "Errors"
msgstr "பிழைகள்"

#: ../../english/international/l10n/menu.def:30
msgid "POT files"
msgstr "பிஓடி கோப்புகள்"

#: ../../english/international/l10n/menu.def:34
msgid "Hints for translators"
msgstr "மொழிபெயர்ப்பாளரகளுக்கான குறிப்புகள்"

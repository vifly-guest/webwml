#use wml::debian::template title="Debian-Installer errata"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="5863207a091bcfec9124de413b37739d67e43557" mindelta="1" maxdelta="1"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

<h1>Kända problem i <humanversion /></h1>

<p>
Detta är en lista på kända problem i utgåvan <humanversion /> av
Debian Installer. Om du inte ser ditt problem i listan här, vänligen sänd in en
<a href="$(HOME)/releases/stable/amd64/ch05s04.html#submit-bug">installationsrapport</a>
som beskriver problemet.
</p>

<dl class="gloss">

	<dt>GNOME kan misslyckas att starta med vissa inställningar i virtuella
	maskiner.</dt>
	<dd>Det uppmärksammades under testning av avbildningar av Stretch Alpha 4
	att GNOME kan misslyckas att starta beroende på inställningen som används
	för virtuella maskiner. Det verkar som om det är ok att använda cirrus som
	emulerad videokrets.
	<br />
	<b>Status:</b> Under utredning.</dd>
	
	<dt>Skrivbordsinstallationer fungerar möjligen inte med hjälp av endast CD#1</dt>
	<dd>
		Tack vare utrymmesbrist på den första CDn, så får inte alla väntade delar
		av GNOME-skrivbordet plats på CD#1. Använd extra paket-källor (t.ex. en
		andra CD eller en nätverksspegel) för en framgångsrik installation,
		<br />
		<b>Status:</b> Det är osannolikt att fler insatser kommer göras för att få
		fler paket att få plats på CD#1.
	</dd>
	
	<dt>Temat som används i installeraren</dt>
	
	<dd>Det finns ingen artwork för Buster ännu, och installeraren använder
	ännu Stretch-temat.
	<br />
	<b>Status:</b>
		Rättat i Buster RC 1: <a href="https://bits.debian.org/2019/01/futurePrototype-will-be-the-default-theme-for-debian-10.html">futurePrototype</a> har integrerats.
	</dd>

	<dt>Felaktiga genvägar i redigeraren nano</dt>
	<dd>Versionen av nano som används i Debian Installer visar inte
		samma genvägar som det vanliga nanopaketet (på installerade
		system). Särskilt förslaget att använda <tt>C-s</tt> för att
		<q>Spara</q> verkar vara ett problem för personer som
		använder installeraren på en seriell konsol
		(<a href="https://bugs.debian.org/915017">#915017</a>).
		<br />
		<b>Status:</b> Rättat i Buster RC 1.</dd>

	<dt>TLS-stöd i wget är trasigt</dt>
	<dd>En förändring i biblioteket openssl leder till vissa problem för
		dess användare, på grund av att en konfigurationsfil som ursprungligen
		var valfri plötsligt blev obligatorisk. Som en konsekvens av detta
		stödjer åtminstone <tt>wget</tt> inte HTTPS i installeraren
		(<a href="https://bugs.debian.org/926315">#926315</a>)
	<br />
	<b>Status:</b> Rättat i Buster RC 2.</dd>

	<dt>Entropi-relaterade problem</dt>
	<dd>Även med funktionellt HTTPS-stöd i wget kan man råka ut för
		entropi-relaterade problem, beroende på tillgängligheten
		av en hårdvaru-baserad slumptalsgenerator (RNG) och/eller
		ett tillräckligt stort antal händelser för att fylla
		entropipoolen (<a href="https://bugs.debian.org/923675">#923675</a>).
		Vanliga symptom kan vara en lång fördröjning när den första
		HTTPS-anslutningen sker, eller under generering av SSH-nyckelpar.
	<br />
	<b>Status:</b> Rättat i Buster RC 2.</dd>

	<dt>LUKS2 är inkompatibelt med GRUB's kryptodiskstöd</dt>
	<dd>Det upptäckts bara nyligen att GRUB inte har stöd för
		LUKS2. Detta betyder att användare som vill använda
		<tt>GRUB_ENABLE_CRYPTODISK</tt> och undvika en separat,
		icke krypterad <tt>/boot</tt>, inte kommer att göra detta
		(<a href="https://bugs.debian.org/927165">#927165</a>).
		Denna setup stöds inte i installeraren hursomhelst, men det
		vore förnuftigt att åtminstone dokumentera denna begränsning
		mer framträdande, och åtminstone ha möjligheten att välja
		LUKS1 under installationsprocessen.
	<br />
	<b>Status:</b> Några idéer har uttryckts i felrapporten. De ansvariga för cryptsetup har skrivit <a href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">specifik dokumentation</a>.</dd>
	
	<dt>Misslyckades att preseeda anpassade APT-förråd</dt>
	<dd>Det är vanligt att även specificera extra nycklar när man konfigurerar
	anpassade APT-förråd, med hjälp av parametrar som
	<tt>apt-setup/local0/key</tt>. På grund av förändringar i
	sättet som <tt>apt-key</tt> fungerar kunde försök att specificera en
	sådan inställning resultera i misslyckande med Buster-installeraren
	(<a href="https://bugs.debian.org/851774">851774</a>).
	<br />
	<b>Status:</b>
		Den <a href="https://bugs.debian.org/851774#68">föreslagna patchen</a>
		granskades inte i tid för den initiala Buster-utgåvan, men förväntas
		att testas under Bullseye-utvecklingscykeln, och bakåtanpassas till
		en Busterpunktutgåva så småningom.</dd>

<!-- things should be better starting with Jessie Beta 2...
	<dt>Stöd för GNU/kFreeBSD</dt>

	<dd>
		Installationsavbildningarna för GNU/kFreeBSD påverkas av några
		viktiga fel
		(<a href="https://bugs.debian.org/757985">#757985</a>,
		<a href="https://bugs.debian.org/757986">#757986</a>,
		<a href="https://bugs.debian.org/757987">#757987</a>,
		<a href="https://bugs.debian.org/757988">#757988</a>). Anpassarna skulle
		absolut kunna använda en hjälpande hand när det gäller att få
		installeraren i skick!
	</dd>
-->
	
<!-- kind of obsoleted by the first "important change" mentioned in the 20140813 announce..
	<dt>Tillgänglighet i det installerade systemet</dt>
	
	<dd>
		Även om funktionalitet för tillgänglighet används i under
		installationsprocessen, så kanske detta inte automatiskt är aktiverat i
		det installerade systemet.
	</dd>
-->
	

<!--
	<dt>Potentiella problem med UEFI-uppstart på amd64</dt>
	<dd>Det har rapporterats problem vid uppstart av Debian Installer i UEFI-läge
		på amd64-system. Vissa system startar inte säkert med hjälp av
		<code>grub-efi</code>, och andra visar problem med grafisk korruption när
		den visar den inledande installationssplashskärmen.
	<br />
		Om du stöter på något av dessa problem, vänligen skicka en felrapport och
		ge oss så mycket detaljer som möjligt, både om symptomen och om din
		hårdvara - detta assisterar teamet när dom fixar problemen. Som en
		temporär lösning, försök med att stänga av UEFI och installera med hjälp
		av <q>Legacy BIOS</q> eller <q>Fallback-läge</q> istället.
	<br />
		<b>Status:</b>Fler felrättningar kan komma i de olika punkt-utgåvorna av
		Wheezy.
	</dd>
-->

<!-- leaving this in for possible future use...
	<dt>i386: more than 32 mb of memory is needed to install</dt>
	<dd>
	The minimum amount of memory needed to successfully install on i386
	is 48 mb, instead of the previous 32 mb. We hope to reduce the
	requirements back to 32 mb later. Memory requirements may have
	also changed for other architectures.
	</dd>
-->

</dl>

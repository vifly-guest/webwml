#use wml::debian::translation-check translation="5ff7a077f0e13d9fe9cae52a517d81e9a15c05d7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourrait conduire à une élévation de privilèges, à un déni de service ou à
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14821">CVE-2019-14821</a>

<p>Matt Delco a signalé une situation de compétition dans la fonction
Coalesced MMIO de KVM qui pourrait conduire à un accès hors limites dans le
noyau. Un attaquant local autorisé à accéder à /dev/kvm pourrait utiliser
cela pour provoquer un déni de service (corruption de mémoire ou plantage)
ou éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14835">CVE-2019-14835</a>

<p>Peter Pi de Tencent Blade Team a découvert une absence de vérification
de limites dans vhost_net, le pilote de dorsal de réseau pour les hôtes
KVM, menant à un dépassement de tampon quand l'hôte débute la migration en
direct d'une machine virtuelle. Un attaquant qui contrôle une machine
virtuelle pourrait utiliser cela pour provoquer un déni de service
(corruption de mémoire ou plantage) ou éventuellement pour une élévation de
privilèges sur l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15117">CVE-2019-15117</a>

<p>Hui Peng et Mathias Payer ont signalé une absence de vérification
de limites dans dans le code d'analyse du descripteur du pilote usb-audio,
menant à une lecture hors limites de tampon. Un attaquant capable d'ajouter
des périphériques USB pourrait éventuellement utiliser cela pour provoquer
un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15118">CVE-2019-15118</a>

<p>Hui Peng et Mathias Payer ont signalé une récursion illimitée dans le
code d'analyse du descripteur du pilote usb-audio, menant à un dépassement
de pile. Un attaquant capable d'ajouter des périphériques USB pourrait
utiliser cela pour provoquer un déni de service (corruption de mémoire ou
plantage) ou éventuellement une élévation de privilèges. Sur l'architecture
amd64 ainsi que sur l'architecture arm64 dans Buster, cela est atténué par
une page de protection sur la pile du noyau, ainsi, il est uniquement
possible de provoquer un plantage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15902">CVE-2019-15902</a>

<p>Brad Spengler a signalé qu'une erreur de rétroportage réintroduisait une
vulnérabilité « Spectre v1 » dans la fonction ptrace_get_debugreg() du
sous-système ptrace.</p></li>

</ul>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été
corrigés dans la version 4.9.189-3+deb9u1.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 4.19.67-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4531.data"
# $Id: $

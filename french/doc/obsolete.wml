#use wml::debian::template title="Documentation obsolète"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#use wml::debian::translation-check translation="89c34d78bbbe7a447c9578651b0f0c0e3ba86140" maintainer="Nicolas Bertolissio"

<h1 id="historical">Documents d'archive</h1>

<p>
Les documents listés ci-dessous ont été soit écrits il y a longtemps et ne sont
plus à jour, soit écrits pour des versions précédentes de Debian et n'ont
pas été mis à jour pour la version actuelle. Leurs informations sont
périmées, mais elle peuvent tout de même être intéressantes pour quelques
personnes.
</p>

<p>Les documents ayant perdu leur pertinence et n’étant plus d’aucune utilité,
ont vu leurs références retirées, mais le code source de beaucoup de ces
manuels peut être retrouvé dans le
<a href="https://salsa.debian.org/ddp-team/attic">grenier du Projet de documentation Debian</a>.</p>

<h2 id="user">Documentation pour les utilisateurs</h2>

<document "Documentation dselect pour débutants" "dselect">

<div class="centerblock">

<p>
Ce fichier documente dselect pour les utilisateurs novices et son rôle est
d'aider à installer Debian avec succès. Il n'explique
pas tout, aussi quand vous rencontrerez dselect pour la première fois, naviguez
à travers les écrans d'aide.
</p>

<doctable>
  <authors "Stéphane Bortzmeyer">
  <maintainer "(?)">
  <status>
  au point mort&nbsp;: <a
  href="https://packages.debian.org/aptitude">aptitude</a> a remplacé
  dselect en tant qu'interface standard de gestion des paquets Debian
  </status>
  <availability>
  <inddpcvs name="dselect-beginner" formats="html txt pdf ps"
            langs="ca cs da de en es fr hr it ja pl pt ru sk" vcstype="attic">
  </availability>
</doctable>

</div>


<hr />

<document "Guide de l'utilisateur" "users-guide">

<div class="centerblock">

<p>
Ce guide n'est rien d'autre que le <q>Progeny User's Guide</q>, réécrit et
adapté pour le système Debian standard.
</p>

<p>
Plus de 300&nbsp;pages pédagogiques pour débuter avec le système
Debian, de l'interface graphique à la ligne de commande.
</p>

<doctable>
  <authors "Progeny Linux Systems, Inc.">
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <status>
  Utile en tant que tutoriel, ce manuel a été écrit pour la version
  <em>Woody</em> et il est devenu obsolète.
  </status>
  <availability>
# langs="en" isn't redundant, it adds the needed language suffix to the link
  <inddpcvs name="users-guide" index="users-guide" langs="en" formats="html txt pdf" vcstype="attic">
  </availability>
</doctable>

</div>


<hr />

<document "Cours Debian" "tutorial">

<div class="centerblock">

<p>
Destiné au nouvel utilisateur qui vient d'installer Linux, ce manuel en
facilite la découverte&nbsp;; il conviendra également à l'utilisateur néophyte
d’un système qu'il n'administre pas.
</p>

<doctable>
  <authors "Havoc Pennington, Oliver Elphick, Ole Tetlie, James Treacy,
  Craig Sawyer, Ivan E. Moore II">
  <editors "Havoc Pennington">
  <maintainer "(?)">
  <status>
  au point mort, incomplet ;
  il est peut-être rendu obsolète par le <a href="#quick-reference">\
  Guide de référence pour Debian</a>
  </status>
  <availability>
  incomplet
  <inddpcvs name="debian-tutorial" cvsname="tutorial">
  </availability>
</doctable>

</div>


<hr />

<document "Debian GNU/Linux: Guide to Installation and Usage" "guide">

<div class="centerblock">

<p>
Un manuel orienté vers l'utilisateur.
</p>

<doctable>
  <authors "John Goerzen, Ossama Othman">
  <editors "John Goerzen">
  <status>
  prêt (mais pour <i>Potato</i>)
  </status>
  <availability>
  <inoldpackage "debian-guide">
  </availability>
</doctable>

</div>


<hr />

<document "Manuel de référence de l'utilisateur Debian" "userref">

<div class="centerblock">

<p>
Ce manuel fournit une vue d'ensemble sur tout ce qu'un utilisateur devrait
savoir sur son système Debian GNU/Linux (c'est-à-dire la mise en place de
X Window, comment configurer le réseau, accéder au lecteur de disquettes, etc.).
Son rôle est de combler le fossé entre le Cours Debian et les pages
détaillées de manuel et d'info fournies avec chaque paquet.
</p>

<p>
Il a également été conçu pour donner des idées sur la manière de combiner les
commandes, sur le principe d’Unix, qu'<em>il y a toujours plus d'une façon de
faire la même chose</em>.
</p>

<doctable>
  <authors "Ardo van Rangelrooij, Jason D. Waterman, Havoc Pennington,
      Oliver Elphick, Bruce Evry, Karl-Heinz Zimmer">
  <editors "Thalia L. Hooker, Oliver Elphick">
  <maintainer "(?)">
  <status>
  au point mort et assez incomplet&nbsp;;
  il est peut-être rendu obsolète par le <a href="#quick-reference">\
  Guide de référence pour Debian</a>
  </status>
  <availability>
  <inddpcvs name="user" vcstype="attic">
  </availability>
</doctable>

</div>


<hr />

<document "Manuel de l'administrateur système Debian" "system">

<div class="centerblock">

<p>
Ce document est mentionné dans l'introduction de la Charte Debian. Il couvre
tous les aspects de l'administration d'un système Debian.
</p>

<doctable>
  <authors "Tapio Lehtonen">
  <maintainer "(?)">
  <status>
  au point mort, incomplet&nbsp;;
  il est peut-être rendu obsolète par le <a href="#quick-reference">\
  Guide de référence pour Debian</a>
  </status>
  <availability>
  indisponible
  <inddpcvs name="system-administrator" vcstype="attic">
  </availability>
</doctable>

</div>


<hr />

<document "Manuel de l'administrateur réseau Debian" "network">

<div class="centerblock">

<p>
Ce manuel couvre tous les aspects de l'administration réseau d'un système
Debian.
</p>

<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick, Duncan C. Thomson, Ivan E. Moore II">
  <maintainer "(?)">
  <status>
  au point mort, incomplet&nbsp;;
  il est peut-être rendu obsolète par le <a href="#quick-reference">\
  Guide de référence pour Debian</a>
  </status>
  <availability>
  indisponible
  <inddpcvs name="network-administrator" vcstype="attic">
  </availability>
</doctable>

</div>


<hr />

# Add this to books, there's a revised edition (2nd) @ Amazon
<document "The Linux Cookbook" "linuxcookbook">

<div class="centerblock">

<p>
Ce mode d'emploi du système Debian GNU/Linux montre comment s'en servir
quotidiennement avec efficacité, à travers plus de 1&nbsp;500 recettes allant
de la manipulation de textes, d'images et de sons aux questions de productivité
et de réseaux. Comme les logiciels qu'il décrit, ce livre est libre (NdT&nbsp;:
<em>copylefted</em>) et ses sources sont disponibles.
</p>

<doctable>
  <authors "Michael Stutz">
  <status>
  publié&nbsp;; écrit pour <em>Woody</em>, devenu obsolète
  </status>
  <availability>
  <inoldpackage "linuxcookbook">
  <p><a href="http://dsl.org/cookbook/">en ligne chez l'auteur</a>
  </availability>
</doctable>

</div>

<document "apt-HOWTO" "apt-howto">

<div class="centerblock">
<p>
  Ce manuel tente d'être une source d'information rapide mais néanmoins
  complète sur le système APT et ses caractéristiques. Il contient beaucoup
  d'informations sur les utilisations principales d'APT ainsi que de nombreux
  exemples.
</p>
<doctable>
  <authors "Gustavo Noronha Silva">
  <maintainer "Gustavo Noronha Silva">
  <status>
  obsolète depuis 2009
  </status>
  <availability>
  <inpackage "apt-howto">
  <inddpcvs name="apt-howto" langs="en ca cs de es el fr it ja pl pt-br ru uk tr zh-tw zh-cn"
            formats="html txt pdf ps" naming="locale"  vcstype="attic"/>
  </availability>
</doctable>
</div>


<h2 id="devel">Documentation pour les développeurs</h2>

<document "Introduction&nbsp;: créer un paquet Debian" "makeadeb">

<div class="centerblock">

<p>
Introduction à la création d'un <code>.deb</code> en utilisant
<strong>debmake</strong>.
</p>

<doctable>
  <authors "Jaldhar H. Vyas">
  <status>
  au point mort ; rendu obsolète par le <a href="#maint-guide">Guide des
  nouveaux responsables</a>
  </status>
  <availability>
  <a href="https://people.debian.org/~jaldhar/">en ligne</a>
  </availability>
</doctable>

</div>

<hr />

<document "Manuel des programmeurs Debian" "programmers">

<div class="centerblock">

<p>
Aide les nouveaux développeurs à créer un paquet pour le système Debian
GNU/Linux.
</p>

<doctable>
  <authors "Igor Grobman">
  <status>
  rendu obsolète par le <a href="#maint-guide">Guide des nouveaux
  responsables</a>
  </status>
  <availability>
  <inddpcvs name="programmer" vcstype="attic">
  </availability>
</doctable>

</div>

<hr />

<document "Manuel de l'empaquetage Debian" "packman">

<div class="centerblock">
<p>
  Ce manuel décrit les aspects techniques de la création de paquets binaires
  et de paquets sources Debian. Il documente également l'interface entre
  dselect et ses scripts de méthodes d'accès. Il ne traite pas des exigences
  définies par la Charte du projet Debian et il suppose une certaine
  familiarité avec les fonctions de dpkg du point de vue de l'administration
  d'un système.

<doctable>
  <authors "Ian Jackson, Klee Dienes, David A. Morris, Christian Schwarz">
  <status>
  les parties propres à la Charte ont été intégrées dans
  <a href="devel-manuals#policy">debian-policy</a>.
  </status>
  <availability>
  <inoldpackage "packaging-manual">
  </availability>
</doctable>
</div>

<document "Comment les auteurs de logiciels peuvent distribuer leurs
produits directement au format .deb" "swprod">

<div class="centerblock">
<p>
La lecture de ce document constitue un bon point de départ pour les
auteurs de logiciels qui souhaiteraient savoir comment intégrer leurs
produits à l'intérieur de Debian, quelles sont les différentes situations
et problèmes qui peuvent surgir en fonction des licences du produit et
des choix de l'auteur, et quelles sont les possibilités pour y faire
face. Ce document n'explique pas le processus de création des paquets,
mais il fait référence aux manuels décrivant ce processus.

  <p>Vous devriez le lire si vous voulez vous familiariser et vous faire
  une idée générale de la création et distribution de paquets Debian,
  pour éventuellement y ajouter les vôtres.

<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  obsolète
  </status>
  <availability>
  <inddpsvn-distribute-deb>
  </availability>
</doctable>
</div>

<hr>

<h2 id="misc">Documentation diverse</h2>

<document "Référentiels Debian HOWTO" "repo">

<div class="centerblock">
<p>
  Ce document explique le fonctionnement et la création des référentiels
  Debian, ainsi que la syntaxe précise du fichier <tt>sources.list</tt>.
</p>
<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  prêt (?)
  </status>
  <availability>
  <inddpcvs name="repository-howto" index="repository-howto"
            formats="html" langs="en fr de uk ta" vcstype="attic">
   </availability>
 </doctable>
 </div>

<document "SGML-XML HOWTO" "sgml-howto">

<div class="centerblock">
<p>
  Ce HOWTO contient des informations pratiques sur l'utilisation du SGML et du
  XML sur un système d'exploitation Debian.

<doctable>
  <authors "Stephane Bortzmeyer">
  <maintainer "Stephane Bortzmeyer">
  <status>
  au point mort, obsolète
  </status>
  <availability>
  <inddpcvs name="sgml-howto" formats="html" srctype="SGML" vcstype="attic"/>
  </availability>
</doctable>
</div>
